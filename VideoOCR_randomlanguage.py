import io
import cv2
import sys
import numpy
import random
from PIL import Image
from googletrans import Translator
from google.cloud import vision
from google.cloud.vision import types
filename  = open("Captions.txt",'w')
sys.stdout = filename
translator = Translator()
client = vision.ImageAnnotatorClient()
y=200
x=0
h=800
w=1920
def detect_text(path):
    """Detects text in the file."""
    with io.open(path, 'rb') as image_file:
        content = image_file.read()
    image = types.Image(content=content)
    response = client.text_detection(image=image)
    texts = response.text_annotations
    string = ''
    for text in texts:
        string+=' ' + text.description
    return string    
cap = cv2.VideoCapture(0)
translator = Translator()
random.seed(0)
langs = ['af', 'sq', 'am', 'ar', 'hy', 'az', 'eu', 'be', 'bn', 'bs', 'bg', 'ca', 'ceb', 'ny', 'zh-cn', 'zh-tw', 'co', 'hr', 'cs', 'da', 'nl', 'en', 'eo', 'et', 'tl', 'fi', 'fr', 'fy', 'gl', 'ka', 'de', 'el', 'gu', 'ht', 'ha', 'haw', 'iw', 'he', 'hi', 'hmn', 'hu', 'is', 'ig', 'id', 'ga', 'it', 'ja', 'jw', 'kn', 'kk', 'km', 'ko', 'ku', 'ky', 'lo', 'la', 'lv', 'lt', 'lb', 'mk', 'mg', 'ms', 'ml', 'mt', 'mi', 'mr', 'mn', 'my', 'ne', 'no', 'or', 'ps', 'fa', 'pl', 'pt', 'pa', 'ro', 'ru', 'sm', 'gd', 'sr', 'st', 'sn', 'sd', 'si', 'sk', 'sl', 'so', 'es', 'su', 'sw', 'sv', 'tg', 'ta', 'te', 'th', 'tr', 'uk', 'ur', 'ug', 'uz', 'vi', 'cy', 'xh', 'yi', 'yo', 'zu']
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
	#cv2.imread
    file = 'live.png'
    crop = frame[y:y+h, x:x+w]
    cv2.imwrite(file, crop)    
    # Display the resulting frame
    #cv2.imshow('frame',frame)
    r_lang = (random.choice(langs))
    translation = translator.translate(detect_text(file),dest=(r_lang))
    print ("Language_string: "+str(r_lang)+"  "+translation.text)
    sys.stdout.flush()
cap.release()
cv2.destroyAllWindows()
